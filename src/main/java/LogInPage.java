import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by pabloalcocer on 11/16/16.
 */
public class LogInPage {
    private WebDriver driver;
    private final String email = "cen4072.group3@gmail.com";
    private final String password = "softwaretest";

    public LogInPage(WebDriver driver){
        this.driver = driver;
    }

    public LogInPage navigateToLogIn(){
        new NavigationBar(driver)
                .clickLogin();
        return this;
    }

    public void logInDefaultCredentials(){
        logIn(email, password);
    }

    public void logIn(String email, String password){
        WebElement emailTextBox = driver
                .findElement(By.xpath("//input[@id='email-login']"));
        WebElement passwordTextBox = driver
                .findElement(By.xpath("//input[@id='password-login']"));
        WebElement logInButton = driver
                .findElement(By.xpath("//div[3]/button"));

        emailTextBox.click();
        emailTextBox.sendKeys(email);

        passwordTextBox.click();
        passwordTextBox.sendKeys(password);
    }
}

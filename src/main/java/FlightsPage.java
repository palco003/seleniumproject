import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by pabloalcocer on 11/16/16.
 */
public class FlightsPage {

    private WebDriver driver;
    private WebDriverWait wait;
    private String searchType;
    private WebElement fromTextField;
    private WebElement toTextField;
    private WebElement searchButton;
    private boolean dateFlag = false;
    private String date;

    public FlightsPage(WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
    }

    public FlightsPage navigateToFlights(){
        new NavigationBar(driver)
                .clickFlightLink();

        return this;
    }

    public FlightsPage setDate(String date){
        dateFlag = true;
        this.date = date;
        return this;
    }

    public FlightsPage setFieldsBySearchType(){

        if(searchType.equals("Roundtrip")){
            fromTextField = driver.findElement(By.xpath("//div[3]/form/div/div/div/input"));
            toTextField = driver.findElement(By.xpath("//div[3]/form/div/div/div[2]/input"));
            searchButton = driver.findElement(By.xpath("//div[3]/form/div/button"));
        } else if (searchType.equals("One-Way")){
            fromTextField = driver.findElement(By.xpath("//div/input"));
            toTextField = driver.findElement(By.xpath("//div[2]/input"));
            searchButton = driver.findElement(By.xpath("//button"));
        } else if (searchType.equals("Multi-City")){
            searchType = "Multi-City";
        } else{
            searchType = "Price Graph";
        }
        return this;
    }

    public void selectDate(){
        WebElement dateTo;
        WebElement dateFrom;
        if(dateFlag && searchType.equals("One-Way")){
            dateTo = driver.findElement(By.xpath("//div[3]/input"));
            dateTo.click();
            dateTo.sendKeys(date);
        }
    }

    public FlightsPage setPassengers(int adult, int children, int senior, int infant, int lapInfant){
        WebElement passengerField = driver.findElement(By.xpath("//div/div/div[4]/div"));
        passengerField.click();
        WebElement adultMinus = driver.findElement(By.xpath("//div[2]/div/button"));
        WebElement adultPlus = driver.findElement(By.xpath("//button[2]"));
        WebElement infantPlus = driver.findElement(By.xpath("//div[4]/div[2]/div/button[2]"));
        for(int i = 1; i < adult && i < 6; i++){
            adultPlus.click();
        }
        if (adult == 0 && infant == 1){
            adultMinus.click();
            infantPlus.click();
        } else if (adult == 0){
            passengerField.click();
            adultMinus.click();
        }
        return this;
    }

    public FlightsPage searchForFlights(String from, String to){

        fromTextField.clear();
        fromTextField.click();
        fromTextField.sendKeys(from);

        toTextField.clear();
        toTextField.click();
        toTextField.sendKeys(to);

        selectDate();

        searchButton.click();

        return this;
    }

    public FlightsPage selectSearchType(String type){
        WebElement radioButton;
        if(type.equals("Roundtrip")){
            radioButton = driver.findElement(By.xpath("//label[1]/input"));
            searchType = "Roundtrip";
        } else if (type.equals("One-Way")){
            radioButton = driver.findElement(By.xpath("//label[2]/input"));
            searchType = "One-Way";
        } else if (type.equals("Multi-City")){
            radioButton = driver.findElement(By.xpath("//label[3]/input"));
            searchType = "Multi-City";
        } else{
            radioButton = driver.findElement(By.xpath("//label[4]/input"));
            searchType = "Price Graph";
        }
        radioButton.click();
        return this;
    }

    public String getErrorMessage(){

        WebElement errorMessage = null;
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//form/div[2]/div")));
        if(driver.findElement(By.xpath("//form/div[2]/div")).isDisplayed()){
            errorMessage = driver.findElement(By.xpath("//form/div[2]/div"));
        } else if (driver.findElement(By.xpath("//form/div[3]/div")).isDisplayed()){
            errorMessage = driver.findElement(By.xpath("//form/div[3]/div"));
        }

        return errorMessage.getText();
    }
}

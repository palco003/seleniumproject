import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by pabloalcocer on 11/16/16.
 */
public class CarsPage {
    private WebDriver driver;

    public CarsPage(WebDriver driver){
        this.driver = driver;
    }

    public CarsPage navigateToCars(){
        new NavigationBar(driver)
                .clickCarLink();
        return this;
    }

    public void searchForCars(String pickup){
        WebElement pickupTextField = driver.findElement(By.xpath("//form/div[2]/div/div/input"));
        WebElement searchButton = driver.findElement(By.xpath("//div[2]/button"));

        pickupTextField.clear();
        pickupTextField.click();
        pickupTextField.sendKeys(pickup);

        searchButton.click();
    }
}

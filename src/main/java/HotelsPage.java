import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

/**
 * Created by Crivi002 on 11/16/16.
 */
public class HotelsPage {

    private WebDriver driver;

    public HotelsPage(WebDriver driver) {

        this.driver = driver;
    }

    public HotelsPage navigateToHotels() {

        new NavigationBar(driver)
                .clickHotelLink();
        return this;
    }


    public HotelsPage searchForHotels() {
        WebElement searchButton = driver.findElement(By.className("HotelSearchForm__search"));

        searchButton.click();

        return this;
    }

    public HotelsPage fillWhere(String where) {
        WebElement whereTextField = driver.findElement(By.name("where"));
        whereTextField.clear();
        whereTextField.click();
        whereTextField.sendKeys(where);

        return this;
    }

    public HotelsPage selectHotelDate(String fromDate, String toDate) {
        WebElement firstDateField = driver.findElement(By.xpath("//div[2]/div/form/div/div/div[2]/input"));
        firstDateField.clear();
        firstDateField.click();
        firstDateField.sendKeys(fromDate);
        WebElement secondDateField = driver.findElement(By.xpath("//div[@id='frontpage']/div/div/div[2]/div/div[5]/div[2]/div/div[2]/div/form/div/div/div[3]/input"));
        secondDateField.clear();
        secondDateField.click();
        secondDateField.sendKeys(toDate);
        return this;
    }


    public String getErrorMessage() {

        String xPath = "//div[@id='frontpage']/div/div/div[2]/div/div[5]/div[2]/div/div[2]/div/form/div[2]/div";
        String xPathTwo = "//div[2]/div/form/div[2]/div";
        String xPathThree = "/html/body/div[7]/div/div/div/div/div[1]/div[2]/div/div[5]/div[2]/div[1]/div[2]/div/form/div[2]";
        WebElement errorMessage = null;

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        if (driver.findElement(By.xpath(xPath)).isDisplayed())
            errorMessage = driver.findElement(By.xpath(xPath));
        if (driver.findElement(By.xpath(xPathTwo)).isDisplayed())
            errorMessage = driver.findElement(By.xpath(xPathTwo));
        if(driver.findElement(By.xpath(xPathThree)).isDisplayed())
            errorMessage = driver.findElement(By.xpath(xPathThree));



        return errorMessage.getText();

    }


}
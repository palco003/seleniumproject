import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by pabloalcocer on 11/16/16.
 */
public class PackagesPage {
    private WebDriver driver;

    public PackagesPage(WebDriver driver){
        this.driver = driver;
    }

    public PackagesPage navigateToPackages(){
        new NavigationBar(driver)
                .clickPackageLink();
        return this;
    }

    public void searchForPackages(String from, String to){
        WebElement fromTextField = driver.findElement(By.xpath("//div[4]/div/div[2]/div/form/div/div/div/input"));
        WebElement toTextField = driver.findElement(By.xpath("//div[4]/div/div[2]/div/form/div/div/div[2]/input"));
        WebElement searchButton = driver.findElement(By.xpath("//div[4]/div/div[2]/div/form/div/button"));

        fromTextField.clear();
        fromTextField.click();
        fromTextField.sendKeys(from);

        toTextField.clear();
        toTextField.click();
        toTextField.sendKeys(to);

        searchButton.click();
    }
}

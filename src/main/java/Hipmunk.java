import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by pabloalcocer on 11/16/16.
 */
public class Hipmunk {
    private final String URL = "http://www.hipmunk.com";
    private final String username = "cen4072.group3@gmail.com";
    private final String password = "softwaretest";

    private WebDriver driver;
    private WebDriverWait wait;

    public Hipmunk(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
    }

    public Hipmunk open() {
        driver.get(URL);
        return this;
    }

    public void signinDefaultCredentials() {
        signin(username, password);
    }

    public void signin(String username, String password) {
        WebElement userNameTextField = driver.findElement(By.name("login"));
        WebElement passwordTextField = driver.findElement(By.name("password"));
        WebElement submitButton = driver.findElement(By.cssSelector("input[type=\"image\"]"));

        userNameTextField.clear();
        userNameTextField.sendKeys(username);

        passwordTextField.clear();
        passwordTextField.sendKeys(password);

        submitButton.click();
    }

    public void closePopUp(){
        if(driver.findElement(By.xpath(".//*[@id='lightbox-scroll']")).isDisplayed()){
            WebElement closePopUp = driver.findElement(By.className("hipfont-close"));
            closePopUp.click();
        }
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='lightbox-scroll']/div/div/div[1]/div")));
    }
}

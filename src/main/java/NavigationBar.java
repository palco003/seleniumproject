import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by pabloalcocer on 11/16/16.
 */
public class NavigationBar {
    private WebDriver driver;
    private WebDriverWait wait;

    public NavigationBar(WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
    }

    public void clickFlightLink(){
        wait.until(ExpectedConditions
                .presenceOfElementLocated(By.xpath("//a[contains(@href, '/flights')]")));
        WebElement flightLink = driver
                .findElement(By.xpath("//a[contains(@href, '/flights')]"));
        flightLink.click();
//        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void clickHotelLink(){
        wait.until(ExpectedConditions
                .presenceOfElementLocated(By.xpath("//a[contains(@href, '/hotels')]")));
        WebElement hotelLink = driver
                .findElement(By.xpath("//a[contains(@href, '/hotels')]"));
        hotelLink.click();
//        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void clickCarLink(){
        wait.until(ExpectedConditions
                .presenceOfElementLocated(By.xpath("//a[contains(@href, '/cars')]")));
        WebElement carLink = driver
                .findElement(By.xpath("//a[contains(@href, '/cars')]"));
        carLink.click();
//        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void clickPackageLink(){
        wait.until(ExpectedConditions
                .presenceOfElementLocated(By.xpath("//a[contains(@href, '/packages')]")));
        WebElement packageLink = driver
                .findElement(By.xpath("//a[contains(@href, '/packages')]"));
        packageLink.click();
//        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void clickLogin(){
        WebElement logInLink = driver
                .findElement(By.xpath("//td[2]/div/div[2]/div"));

        logInLink.click();

        WebElement logInAccount = driver
                .findElement(By.xpath("//form[2]/div[5]/span[3]"));

        logInAccount.click();
    }

    public void clickLogo(){
        WebElement homepage = driver.findElement(By.className("hiplogo"));
        homepage.click();
//        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
}
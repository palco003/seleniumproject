import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
* created by crivi002
*/
@RunWith(JUnit4.class)
public class HotelsTest {

    private static DesiredCapabilities capabilities = DesiredCapabilities.firefox();
    private static WebDriver driver;
    private static final String URL = "https://www.hipmunk.com/hotels";

    @BeforeClass
    public static void oneTimeSetup() {
        //capabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
        driver = new FirefoxDriver(capabilities);
        new Hipmunk(driver)
                .open()
                .closePopUp();
    }

    @Before
    public void setup() {

    }

    @After
    public void teardown() {

        driver.navigate().to(URL);
    }

    @AfterClass
    public static void oneTimeTeardown() {

        driver.quit();
    }

    @Test
    public void SearchHotels_ValidCities_CorrectOutput() {

        new HotelsPage(driver)
                .navigateToHotels()
                .fillWhere("Miami")
                .selectHotelDate("Jan 05", "Jan 07")
                .searchForHotels();
    }

    @Test
    public void SearchHotels_CorrectDateInput_CorrectOutput() {
        new HotelsPage(driver)
                .navigateToHotels()
                .fillWhere("Miami")
                .selectHotelDate("Jan 05", "Jan 07")
                .searchForHotels();

    }

    @Test
    public void SearchHotels_EmptyCity_ErrorMessage() {

        String captureErrorMessage = new HotelsPage(driver)
                .navigateToHotels()
                .fillWhere("")
                .selectHotelDate("Jan 05","Jan 07")
                .searchForHotels()
                .getErrorMessage();


        Assert.assertEquals("Please enter a location.", captureErrorMessage);
    }

    @Test
    public void SearchHotels_IncorrectDateInput_ErrorMessage() {
        String Error = new HotelsPage(driver)
                .navigateToHotels()
                .fillWhere("Miami")
                .selectHotelDate("Dec 02", "Jan 31")
                .searchForHotels()
                .getErrorMessage();

        Assert.assertEquals("Why don't you just move there? 30 days is the max.", Error);
    }

    @Test
    public void SearchHotels_IncorrectCity_ErrorMessage(){
        String ErrorMessage = new HotelsPage(driver)
                .navigateToHotels()
                .selectHotelDate("Jan 05","Jan 07")
                .fillWhere("aaabbb")
                .searchForHotels()
                .getErrorMessage();

        Assert.assertEquals("We couldn't find that location.", ErrorMessage);
    }

}

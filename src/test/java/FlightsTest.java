import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Created by pabloalcocer on 11/16/16.
 */
@RunWith(JUnit4.class)
public class FlightsTest {
    private static DesiredCapabilities capabilities = DesiredCapabilities.firefox();
    private static WebDriver driver;
    private static final String URL = "https://www.hipmunk.com";

    @BeforeClass
    public static void oneTimeSetup(){
//        capabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
        driver = new FirefoxDriver(capabilities);
        new Hipmunk(driver)
                .open()
                .closePopUp();
    }

    @Before
    public void setup(){

    }

    @After
    public void teardown(){
        driver.navigate().to(URL);
    }

    @AfterClass
    public static void oneTimeTeardown(){
        driver.quit();
    }

    @Test
    public void searchRoundTripFlights_ValidCities_CorrectOperation(){
        new FlightsPage(driver)
                .navigateToFlights()
                .selectSearchType("Roundtrip")
                .setFieldsBySearchType()
                .searchForFlights("MIA", "JFK");
    }

    @Test
    public void searchRoundTripFlights_EmptyDestination_ErrorMessage(){
        String errorMessage = new FlightsPage(driver)
                .navigateToFlights()
                .selectSearchType("Roundtrip")
                .setFieldsBySearchType()
                .searchForFlights("MIA", "")
                .getErrorMessage();

        Assert.assertEquals("Please enter a location.", errorMessage);
    }

    @Test
    public void searchRoundTripFlights_EmptyFrom_ErrorMessage(){
        String errorMessage = new FlightsPage(driver)
                .navigateToFlights()
                .selectSearchType("Roundtrip")
                .setFieldsBySearchType()
                .searchForFlights("", "JFK")
                .getErrorMessage();

        Assert.assertEquals("Please enter a location.", errorMessage);
    }

    @Test
    public void searchRoundTripFlights_SameFromTo_ErrorMessage(){
        String errorMessage = new FlightsPage(driver)
                .navigateToFlights()
                .selectSearchType("Roundtrip")
                .setFieldsBySearchType()
                .searchForFlights("MIA", "MIA")
                .getErrorMessage();

        Assert.assertEquals("That's an awfully short trip.", errorMessage);
    }

    @Test
    public void searchOneWayFlights_EmptyDate_ErrorMessage(){
        String errorMessage = new FlightsPage(driver)
                .navigateToFlights()
                .selectSearchType("One-Way")
                .setFieldsBySearchType()
                .searchForFlights("MIA", "NYC")
                .getErrorMessage();

        Assert.assertEquals("Select a date please.", errorMessage);
    }

    @Test
    public void searchOneWayFlights_ZeroPeople_ErrorMessage(){
        String errorMessage = new FlightsPage(driver)
                .navigateToFlights()
                .selectSearchType("One-Way")
                .setFieldsBySearchType()
                .setDate("Jan 11")
                .setPassengers(0,0,0,0,0)
                .searchForFlights("MIA", "NYC")
                .getErrorMessage();

        Assert.assertEquals("Please add at least one person.", errorMessage);
    }

    @Test
    public void searchOneWayFlights_InfantOnly_ErrorMessage(){
        String errorMessage = new FlightsPage(driver)
                .navigateToFlights()
                .selectSearchType("One-Way")
                .setFieldsBySearchType()
                .setDate("Jan 11")
                .setPassengers(0,0,0,1,0)
                .searchForFlights("MIA", "NYC")
                .getErrorMessage();

        Assert.assertEquals("Why don't you just mail them?", errorMessage);
    }
}
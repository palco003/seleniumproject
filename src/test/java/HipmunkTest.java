import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Created by pabloalcocer on 11/16/16.
 */
@RunWith(JUnit4.class)
public class HipmunkTest {
    private static DesiredCapabilities capabilities = DesiredCapabilities.firefox();
    private static WebDriver driver;
    private static final String URL = "http://www.hipmunk.com";

    @BeforeClass
    public static void oneTimeSetup(){
        capabilities.setCapability(CapabilityType.ForSeleniumServer.ENSURING_CLEAN_SESSION, true);
        driver = new FirefoxDriver(capabilities);
        new Hipmunk(driver)
                .open()
                .closePopUp();
    }
    @Before
    public void setup(){

    }

    @After
    public void teardown(){
        driver.navigate().to(URL);
    }
    @AfterClass
    public static void oneTimeTeardown(){
        driver.quit();
    }

    @Test
    public void searchHotel_ValidCities_CorrectOperation(){
        new HotelsPage(driver)
                .navigateToHotels()
                .searchForHotels("New York");
    }

    @Test
    public void searchCar_ValidDates_CorrectOperation(){
        new CarsPage(driver)
                .navigateToCars()
                .searchForCars("JFK");
    }

    @Test
    public void searchPackage_ValidDates_CorrectOperation(){
        new PackagesPage(driver)
                .navigateToPackages()
                .searchForPackages("LAX", "ORD");
    }

    @Test
    public void logIn_ValidLogIn_CorrectOperation(){
        new LogInPage(driver)
                .navigateToLogIn()
                .logInDefaultCredentials();
    }
}
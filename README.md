# README #

### Submission ###
Our final tests for submission are under the **master** branch

### What is this repository for? ###

* Group 3 Software Testing project.

### How do I get set up? ###

* Must use Firefox 47.0.1 or before.

### What's what? ###

* There are several tests for Flights done. These are found in **FlightsTest**.
* There is one test for hotel, car, package, and login each in **HipmunkTest**.
* Feel free to use these as a base. 
* Take note of each respective class for each feature. Use the Object Page design pattern. 
* If you need help with Selenium, I recommend a class on Lynda.com with information of SeleniumIDE and WebDriver.